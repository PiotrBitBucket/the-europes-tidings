describe('Environmental variables', () => {
  const OLD_ENV = process.env;

  beforeEach(() => {
    jest.resetModules();
    process.env = { ...OLD_ENV };
  });

  afterAll(() => {
    process.env = OLD_ENV;
  });

  test('Will receive process.env variables', () => {
    expect(process.env.NEXT_PUBLIC_ENVIRONMENT).toBe('test');
  });
});