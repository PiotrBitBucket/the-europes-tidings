const { MongoClient } = require('mongodb');
const { MongoMemoryServer } = require('mongodb-memory-server');

class MongoMemory {
  static #connection;
  static #mongoServer;
  static #database;

  static async connect(dbName = '') {
    this.#mongoServer = await MongoMemoryServer.create({
      instance: {
        dbName,
      }
    });

    this.#connection = await MongoClient.connect(this.#mongoServer.getUri(), {});
  }

  static async getDatabase() {
    this.#database = this.#connection.db(this.#mongoServer.instanceInfo.dbName);
    return this.#database;
  }

  static async close() {
    if(this.#database) {
      this.#database.dropDatabase();
    }

    if (this.#mongoServer) {
      await this.#mongoServer.stop();
    }
    if (this.#connection) {
      await this.#connection.close();
    }
  }
}

module.exports = MongoMemory;