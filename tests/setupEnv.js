import { loadEnvConfig } from '@next/env'

async function setup() {
  const projectDir = process.cwd()
  loadEnvConfig(projectDir)
}

export default setup;