const MongoMemory = require('tests/helpers/MongoMemory');

async function insertOneUser(db) {
  const users = db.collection('users');
  const mockUser = {name: 'John', age: 23};
  await users.insertOne(mockUser);
}

describe('MONGODB:', () => {
  describe('CRUD operations', () => {
    let connection;
    let db;

    beforeAll(async () => {
      Promise.all([
        MongoMemory.connect(process.env.MONGODB_DATABASE),
        MongoMemory.getDatabase()
      ]).then(values => {
        [connection, db] = values;
      })
    });

    afterAll(async () => {
      await MongoMemory.close();
    });

    it('Read - should read empty file, add, read inserted data', async () => {
      const before = await db.collection('users').count();
      await insertOneUser(db);
      const after = await db.collection('users').count();

      expect([before, after]).toEqual([0,1]);
    });

    it('Create - should insert a doc into collection', async () => {
      await insertOneUser(db);

      const insertedUser = await db.collection('users').findOne({name: 'John'});
      delete insertedUser._id;
      expect(insertedUser).toEqual({name: 'John', age: 23});
    });

    it('Update - should update collection', async () => {
      await insertOneUser(db);
    });
  });

  describe('Advanced operations', () => {
    // todo delete, insert many, read limited number, read with filters, update many, delete many
    // todo fix problem with no flushing database after each test
  })
});

