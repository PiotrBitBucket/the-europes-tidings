## Getting Started

First, run the development server:

```bash
npm run dev
```

## Tests and quality
```bash 
npm run test
npm run lint
```

## Environment files
- `.env` file is a default for all environment files
- `.env.test` overwriting `.env` configuration in test environment
- `.env.development` overwriting `.env` configuration in development environment
- `.env.production` overwriting `.env` configuration in production environment

## Databases
- `mongodb-memory-server` is used in tests

## API
### GraphQl
`URL: api/graphql`
 - ApolloClient is used to connect next.js and graphQl
