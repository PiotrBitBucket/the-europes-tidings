import { configureStore } from '@reduxjs/toolkit'
// @ts-ignore
import counterReducer from '/src/redux/reducers/counter'

export const store = configureStore({
  reducer: {
    counter: counterReducer,
  },
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch