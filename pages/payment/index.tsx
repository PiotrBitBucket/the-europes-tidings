import React from 'react';
import {PayPalScriptProvider, PayPalButtons} from "@paypal/react-paypal-js";

;

export default function Payments() {
  const initialOptions = {
    'client-id': 'AQbuOQ6Kj-1DpKOT8E3C6cJnwvCUNnZV-rPOOnmpblFEwB_gY9xuHLR7e8TNGBqdu-HeuIOXpkcXNBS4',
  };

  return (
    <PayPalScriptProvider options={initialOptions}>
      <PayPalButtons style={{ layout: "horizontal" }} />
    </PayPalScriptProvider>
  )
}
