export default function About({articles}) {
  return (
    <>
      {articles.map((articles, index) => (
        <h3 key={index}>{articles.title}</h3>
      ))}
    </>
  );
}


export const getStaticProps = async () => {
  const res = await fetch(`https://jsonplaceholder.typicode.com/posts?_limit=6`);
  const articles = await res.json();

  return {
    props: {
      articles
    }
  }
}