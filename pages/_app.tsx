import Head from 'next/head';
import Container from '@mui/material/Container';
import {ThemeProvider} from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import {createGenerateClassName, StylesProvider} from '@mui/styles';
import {FunctionComponent, useState} from 'react';
import {Provider as AuthProvider} from 'next-auth/client';
import {AppProps} from 'next/app';
import {Provider as ReduxProvider} from 'react-redux';
// @ts-ignore
import {store} from '/src/redux/store';
// @ts-ignore
import theme from '/components/themes/standard/Theme';
// @ts-ignore
import Navbar from '/components/layout/standard/Navbar';
// @ts-ignore
import Footer from '/components/layout/standard/Footer';

const generateClassName = createGenerateClassName({
  productionPrefix: 'pp-'
});

const App: FunctionComponent<AppProps> = ({ Component, pageProps }) => {
  const [key] = useState(0);

  /** @see /docs/user/authentication.md */
  const authOptions = {
    clientMaxAge: 0,
    keepAlive: 0
  };

  return (
    <>
      <Head>
        <title>Home</title>
        <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width"/>
      </Head>
      <AuthProvider options={authOptions} session={pageProps.session}>
        <ReduxProvider store={store}>
          <StylesProvider key={key} generateClassName={generateClassName}>
            <ThemeProvider theme={theme}>
              <CssBaseline/>
              <Container maxWidth="lg">
                <Navbar/>
                <main>
                  <Component {...pageProps} />
                </main>
                <Footer/>
              </Container>
            </ThemeProvider>
          </StylesProvider>
        </ReduxProvider>
      </AuthProvider>
    </>
  );
}

export default App;
