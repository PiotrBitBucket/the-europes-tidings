import {useRouter} from 'next/router'

export default function Section() {
  const router = useRouter();
  const name = router.query.sectionName;
  return (
    <>
      <h1>API</h1>
      <h1>{name}</h1>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus accusantium adipisci amet autem cum distinctio doloribus, earum enim facilis, ipsum iure laudantium praesentium rem sit voluptas! Dignissimos dolor doloremque perspiciatis.</p>
    </>
  );
}

