const { MongoClient } = require('mongodb');
const db = {
  uri: 'mongodb://the-europes-tidings:secret@localhost:27017/the-europes-tidings',
  name: 'the-europes-tidings',
}


async function handler(req, res) {
  if (req.method === 'POST') {
    // const data = req.body;
    const client = new MongoClient(db.uri, { useNewUrlParser: true, useUnifiedTopology: true });
    client.connect(data => {
      const articles = client.db(db.name).collection('articles');
      client.close();

      res.status(201).json({message: 'Article inserted!'})
    });

    // res.status(500).json({message: 'Internal server error'})
  }
}

export default handler;


