import {gql, ApolloServer} from "apollo-server-micro";
import {ApolloServerPluginLandingPageGraphQLPlayground} from "apollo-server-core";

const articles = [
  {
    title: 'first article',
    lead: 'first-article',
    likes: 0,
    gallery: { // Gallery obj
      exist: true,
      type: 'STATIC',
    },
    content: {
      main: [
        {name: 'this is first main paragraph'},
        {name: 'this is second yee main paragraph'}
      ],
      forPhotos: [
        {name: 'this is first photo paragraph'},
        {name: 'this is paragraph for second photo'}
      ],
    },
    created_at: dayjs('2019-01-25').format(),
  },
  {
    title: 'second article',
    lead: 'second-article',
    likes: 0,
    gallery: { // Gallery obj
      exist: true,
      type: 'STATIC',
    },
    content: {
      main: [
        {name: 'this is second main paragraph'},
        {name: 'this is second yee main paragraph'}
      ],
      forPhotos: [
        {name: 'this is second photo paragraph'},
        {name: 'this is paragraph for second photo'}
      ],
    },
    created_at: dayjs('2019-01-24').format(),
  }
]

const typeDefs = gql`
    enum GalleryTypeEnum {
        STATIC
        DYNAMIC
    }
    
    type Gallery {
        exist: Boolean
        type: GalleryTypeEnum
    }
    
    type ArticleMainContent {
        name: String
    }

    type ArticlePhotoContent {
        name: String
    }

    type ArticleContent {
        main: [ArticleMainContent!]!
        forPhotos: [ArticlePhotoContent!]!
    }

    type Article {
        id: ID
        title: String
        lead: String
        likes: Int
        gallery: Gallery
        content: ArticleContent
    }
    
    type Query {
        getArticle: Article
    }
`;

const resolvers = {
  Query: {
    getArticle: () => {
      return articles[0];
    },
  },
};

const apolloServer = new ApolloServer({
  typeDefs,
  resolvers,
  playground: true,
  introspection: true,
  plugins: [ApolloServerPluginLandingPageGraphQLPlayground()],
});

const startServer = apolloServer.start();

export default async function handler(req, res) {
  await startServer;
  await apolloServer.createHandler({
    path: "/api/graphql",
  })(req, res);
}

export const config = {
  api: {
    bodyParser: false,
  },
};