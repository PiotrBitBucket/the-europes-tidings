import Box from '@mui/material/Box';
import OneTwoContainer from '/components/layout/article-containers/OneTwoContainer';
import { ApolloClient, InMemoryCache, gql } from '@apollo/client';
require('/src/bootstrap');

export default function Home({ articles }) {
  console.log(articles)

  return (
    <>
      <Box sx={{flexgrow: 1}}>
        <Box sx={{ bgcolor: '#7bc4f8', height: '100vh', width: '100%' }} >
          <OneTwoContainer/>
        </Box>
      </Box>
    </>
  );
}

export async function getStaticProps() {
  const client = new ApolloClient({
    uri: 'https://api.spacex.land/graphql/',
    cache: new InMemoryCache()
  });

  const { data } = await client.query({
    query: gql`
    query GetLaunches {
      launchesPast(limit: 10) {
        id
        mission_name
      }
    }
  `
  });
console.log(data)
  return {
    props: {
      articles: []
    }
  }
  // Code will go here
}