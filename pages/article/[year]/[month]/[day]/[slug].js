import {useRouter} from 'next/router'

export default function Slug() {
  const router = useRouter();
  const slug = router.query.slug || 'No slug found';
  const year = router.query.year;
  const month = router.query.month;
  const day = router.query.day;
  return (
    <>
      <h1>{slug}</h1>
      <p>{year}-{month}-{day}</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus accusantium adipisci amet autem cum distinctio doloribus, earum enim facilis, ipsum iure laudantium praesentium rem sit voluptas! Dignissimos dolor doloremque perspiciatis.</p>
    </>
  );
}