import type {Config} from '@jest/types';

async function config(): Promise<Config.InitialOptions> {
  return {
    globalSetup: '<rootDir>/tests/setupEnv.js',
    verbose: true,
    roots: [
      "<rootDir>",
    ],
    modulePaths: [
      "<rootDir>",
    ],
    moduleDirectories: [
      "node_modules"
    ],
  };
}

export default config;