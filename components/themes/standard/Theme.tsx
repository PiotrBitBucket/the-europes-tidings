import { createTheme } from '@mui/material/styles';

const theme = createTheme({
  palette: {
    divider: 'black',
    primary: {
      main: '#4770b9',
    },
    secondary: {
      main: '#48494c',
    },
  },
});

export default theme;