import {signIn, signOut, useSession} from 'next-auth/client'
import styles from './UserAvatar.module.css';
import AccountCircle from '@mui/icons-material/AccountCircle';
import Avatar from '@mui/material/Avatar';
import Chip from '@mui/material/Chip';
import Stack from '@mui/material/Stack';

const LoggedUserState = ({onClick, session}) => {
  return (
    <>
      <div className={styles.avatarSpace}>
        {session.user.image && <Avatar className={styles.avatar} alt={session.user.name} src={session.user.image}/>}
        {!session.user.image && <AccountCircle className={styles.fullSize}/>}
      </div>

      <div className={styles.row}>
        <div className={styles.fullWidth}><strong>{session.user.name}</strong></div>
        <div className={styles.fullWidth}>{session.user.email}</div>
      </div>

      <div className={styles.buttonContainer}>
        <Chip style={{padding: '0 50px'}} label="Sign out" variant="outlined" onClick={signOut}/>
      </div>
    </>
  );
}

const GuestState = () => {
  const register = () => {
    console.log('Register method is not added!')
  }

  return (
    <>
      <div className={styles.buttonContainer}>
        <Stack direction="row" spacing={1}>
          <Chip style={{padding: '0 25px'}} label="Sign In" variant="outlined" onClick={signIn}/>
          <Chip style={{padding: '0 25px'}} label="Register" variant="outlined" onClick={register}/>
        </Stack>
      </div>
    </>
  )
};

export default function UserAvatar({onClick}) {
  const [session, loading] = useSession()

  return (
    <>
      {session && <LoggedUserState onClick={onClick} session={session}/>}
      {!session && <GuestState/>}
    </>
  );
}
