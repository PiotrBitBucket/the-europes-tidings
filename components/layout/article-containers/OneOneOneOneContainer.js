import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';

export default function OneOneOneOneContainer() {
  return (
    <Grid container spacing={2}>
      <Grid item xs={12} sm={8}>
        <Box sx={{ bgcolor: '#cfe8fc', height: '60vh', width: '100%' }} />
      </Grid>
    </Grid>
  )
}