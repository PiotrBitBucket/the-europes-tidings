import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';

export default function OneTwoContainer() {
  return (
    <Grid container spacing={2}>
      <Grid item xs={12} sm={8}>
        <Box sx={{ bgcolor: '#cfe8fc', height: '60vh', width: '100%' }} />
      </Grid>
      <Grid sx={{height: '60vh'}} container item xs={12} sm={4} spacing={2}>
        <Grid item xs={6} sm={12}>
          <Box sx={{ bgcolor: '#cfe8fc', height: '100%', width: '100%' }} />
        </Grid>
        <Grid item xs={6} sm={12}>
          <Box sx={{ bgcolor: '#cfe8dd', height: '100%', width: '100%' }} />
        </Grid>
      </Grid>
    </Grid>
  )
}