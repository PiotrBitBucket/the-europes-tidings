import {Component} from 'react';
import IconButton from '@mui/material/IconButton';
import Divider from '@mui/material/Divider';
import AppsIcon from '@mui/icons-material/Apps';
import CloseIcon from '@mui/icons-material/Close';
import menuStyles from './HandyMenu.module.css';
import UserAvatar from '/components/auth/header/UserAvatar';
import CategoryMasonry from '/components/layout/standard/CategoryMasonry';
import Popover from '@mui/material/Popover';

function HandyMenuDialog({onClose, open, anchorEl}) {
  const categories = [
    {
      title: 'Technology',
      href: 'technology',
      extension: 'jpg',
      height: 168,
      alt: 'Image by Pete Linforth from Pixabay'
    },
    {title: 'Design', href: 'design', extension: 'png', height: 80, alt: 'Image by Kenny Elijah from Pixabay'},
    {title: 'Culture', href: 'culture', extension: 'jpg', height: 80, alt: 'Image by Igor Ovsyannykov from Pixabay'},
    {title: 'Business', href: 'business', extension: 'jpg', height: 80, alt: 'Image by Pexels from Pixabay'},
    {title: 'Style', href: 'style', extension: 'jpg', height: 168, alt: 'Image by Claudio_Scott from Pixabay'},
    {title: 'Politics', href: 'politics', extension: 'jpg', height: 80, alt: 'Image by Gerd Altmann from Pixabay'},
    {title: 'Opinion', href: 'opinion', extension: 'png', height: 80, alt: 'Image by 愚木混株 Cdd20 from Pixabay'},
    {title: 'Science', href: 'science', extension: 'jpg', height: 80, alt: 'Image by WikiImages from Pixabay'},
    {title: 'Health', href: 'health', extension: 'jpg', height: 80, alt: 'Image by marionbrun from Pixabay'},
    {title: 'Travel', href: 'travel', extension: 'jpg', height: 80, alt: 'Image by Dariusz Sankowski from Pixabay'},
  ];

  return (
    <Popover
      id='handy-menu'
      open={open}
      anchorEl={anchorEl}
      onClose={onClose}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'left',
      }}
    >
      <div className={menuStyles.insideContainer}>
        <div className={menuStyles.headline}>
          <div className={menuStyles.space}>
            <IconButton color="secondary" onClick={onClose}><CloseIcon/></IconButton>
          </div>
          <div className={menuStyles.companyName}>
            EuroTid
          </div>
          <div className={menuStyles.space}/>
        </div>

        <Divider/>

        <div className={menuStyles.avatarRow}>
          <UserAvatar/>
        </div>

        <div className={menuStyles.categoriesRow}>
          <CategoryMasonry categories={categories}/>
        </div>

        <hr/>

      </div>
    </Popover>
  )
}

class HandyMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      anchorEl: null,
    };
  }

  handleOpen(event) {
    this.setState({open: true, anchorEl: event.currentTarget});
  }

  render() {
    return (
      <>
        <IconButton
          color="primary"
          aria-label="Open handy menu"
          onClick={(e) => this.handleOpen(e)}
        >
          <AppsIcon/>
        </IconButton>
        <HandyMenuDialog
          open={this.state.open}
          onClose={() => this.setState({open: false})}
          anchorEl={this.state.anchorEl}
        />
      </>
    );
  }
}

export default HandyMenu;