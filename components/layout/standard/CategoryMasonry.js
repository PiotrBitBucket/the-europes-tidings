import Box from '@mui/material/Box';
import { styled } from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import Masonry from '@mui/lab/Masonry';
import Link from '@mui/material/Link';

const heights = [168, 80, 80, 80, 168, 80, 80, 80, 80, 80];

const Item = styled(Paper)(({ theme }) => ({
  color: theme.palette.text.secondary,
  border: '1px solid black',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
}));

export default function CategoryMasonry({categories}) {
  return (
    <>
      <Box sx={{ width: '100%', margin: '0 auto', minHeight: 253 }}>
        <Masonry columns={3} spacing={1} sx={{margin: 0}}>
          {categories.map(({height, title, image, href, extension}, index) => (
            <Link key={index} href={href}>
              <Item sx={{ height }} style={{backgroundImage: `url(/images/categories/${title}.${extension})`}}>
                <div style={{color: 'white'}}>{title}</div>
              </Item>
            </Link>
          ))}
        </Masonry>
      </Box>
    </>
  );
}