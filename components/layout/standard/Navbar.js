import HandyMenu from './HandyMenu';
import navStyle from './Navbar.module.css'

export default function Navbar() {

  return (
    <>
      <div className={navStyle.headline}>
        <div style={{width: '33%'}}/>
        <h2 style={{ margin: 0}}>TheEuropesTidings</h2>
        <div style={{width: '33%', justifyContent: 'end', display: 'flex'}}>
          <HandyMenu/>
        </div>
      </div>
      <hr/>
    </>
  );
}